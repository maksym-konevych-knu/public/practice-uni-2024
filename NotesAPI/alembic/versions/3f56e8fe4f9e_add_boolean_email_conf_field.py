"""add boolean email conf field

Revision ID: 3f56e8fe4f9e
Revises: cd93861a05dd
Create Date: 2023-11-26 19:16:42.201694

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '3f56e8fe4f9e'
down_revision: Union[str, None] = 'cd93861a05dd'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('users_meta', sa.Column('email_confirmed', sa.Boolean(), nullable=False, default=False))


def downgrade() -> None:
    op.drop_column('users_meta', 'email_confirmed')
